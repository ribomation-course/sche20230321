package ribomation.robostore.web;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.domain.RoboProduct;
import ribomation.robostore.jpa.RoboJpaDao;

import java.util.List;

@RestController
@RequestMapping("/api/robo")
public class AjaxRoboController {
    private final RoboJpaDao dao;

    public AjaxRoboController(RoboJpaDao dao) {
        this.dao = dao;
    }

    @GetMapping
    public List<RoboProduct> list() {
        return dao.findAll(Pageable.ofSize(12)).toList();
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String name) {
        dao.deleteById(name);
    }

}
