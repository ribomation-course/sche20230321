package ribomation.robostore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
//@EnableWebMvc
public class RobostoreWeb implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(RobostoreWeb.class, args);
    }

//    @Override
//    public void addViewControllers(ViewControllerRegistry r) {
//        r.addRedirectViewController("/", "/home");
//        r.addViewController("/home").setViewName("home");
//
//        r.addRedirectViewController("/robo", "/robo/list");
//        r.addViewController("/robo/list").setViewName("list");
//        r.addViewController("/robo/create").setViewName("create");
//    }

}
