const baseUrl = 'http://localhost:8080/api/robo'
const json    = 'application/json'
const headers = {'Content-Type': json, 'Accept': json}

export async function loadAll() {
    const res = await fetch(baseUrl)
    return await res.json()
}

export async function loadById(id) {
    const res = await fetch(`${baseUrl}/${id}`)
    return await res.json()
}

export async function remove(id) {
    await fetch(`${baseUrl}/${id}`, {
        method: 'DELETE',
        headers,
    })
}

export async function update(id, delta) {
    const res = await fetch(`${baseUrl}/${id}`, {
        method: 'PUT',
        headers,
        body: JSON.stringify(delta)
    })
    return await res.json()
}

export async function save(person) {
    const res = await fetch(baseUrl, {
        method: 'POST',
        headers,
        body: JSON.stringify(person)
    })
    return await res.json()
}

