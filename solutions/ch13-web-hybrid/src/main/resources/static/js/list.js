import { loadAll, remove } from '/js/rest-client.js'

function populateTR(parentNode, item) {
    const tr = document.createElement('tr')
    parentNode.appendChild(tr)
    tr.innerHTML = `
        <td><img src="${ item.icon }" alt="${ item.name }" class="img-fluid"></td>
        <td><code>${ item.name }</code></td>
        <td>${ item.price } kr</td>
        <td>${ item.count }</td>
        <td>
            <a href="/robo/show/${ item.name }" class="text-decoration-none btn btn-primary">
                <i class="bi bi-person-vcard-fill"></i>
                View
            </a>
            <a href="#" data-name="${ item.name }" class="remove btn btn-danger">
                <i class="bi bi-trash-fill"></i>
                Remove
            </a>
        </td>
    `
    return tr
}

async function removeHdlr(ev) {
    ev.preventDefault()
    const name = ev.target.dataset.name
    await remove(name)
    const tr    = ev.target.parentNode.parentNode
    const tbody = tr.parentNode
    tbody.removeChild(tr)
}

(async () => {
    const items = await loadAll()
    const tbody = document.querySelector('tbody')
    items.forEach(item => populateTR(tbody, item))

    const removeBtns = document.querySelectorAll('.remove')
    removeBtns.forEach(btn => { btn.addEventListener('click', removeHdlr) })
})()

