package ribomation.robostore;

public class RobostoreCLI {
    public static void main(String[] args) {
        var app = new RobostoreCLI();
         app.run(args, new JavaApplicationObjects());
    }

    void run(String[] args, ApplicationObjects app) {
        var cmd = app.getCommands();
        if (args.length == 0) {
            System.err.println("usage: [summary] [list] [one <name>] [delete <name>] [search <phrase>]");
            cmd.summary();
        } else {
            switch (args[0]) {
                case "list" -> cmd.list();
                case "summary" -> cmd.summary();
                case "one" -> cmd.one(args[1]);
                case "delete" -> cmd.delete(args[1]);
                case "search" -> cmd.search(args[1]);
            }
        }
    }

}
