package ribomation.robostore;

import org.h2.jdbcx.JdbcConnectionPool;
import ribomation.robostore.cli.RoboCommands;
import ribomation.robostore.jdbc.BeanPreparator;
import ribomation.robostore.jdbc.JdbcRoboProductDAO;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class ApplicationObjects {

    public DataSource getDataSource() {
        try {
            var props = new Properties();
            props.load(getClass().getResourceAsStream("/db.properties"));
            return JdbcConnectionPool.create(
                    props.getProperty("jdbc.url"),
                    props.getProperty("jdbc.username"),
                    props.getProperty("jdbc.password"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public BeanPreparator getBeanPreparator() {
        return new BeanPreparator();
    }

    public RoboProductDAO getDao() {
        return new JdbcRoboProductDAO(getDataSource(), getBeanPreparator());
    }

    public RoboCommands getCommands() {
        return new RoboCommands(getDao());
    }

}
