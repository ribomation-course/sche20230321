package ribomation.robostore;

import org.h2.jdbcx.JdbcConnectionPool;

import javax.sql.DataSource;

public class H2DataSourceFactory {
    String url, usr, pwd;

    public H2DataSourceFactory(String url, String usr, String pwd) {
        this.url = url;
        this.usr = usr;
        this.pwd = pwd;
    }

    public DataSource create() {
        return JdbcConnectionPool.create(url, usr, pwd);
    }
}
