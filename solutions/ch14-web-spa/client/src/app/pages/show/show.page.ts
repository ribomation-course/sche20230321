import {Component, OnInit} from '@angular/core';
import {CurrencyPipe, JsonPipe, NgIf} from '@angular/common';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import "@angular/common/locales/global/sv";
import {Robot} from "../../domain/robot.domain";
import {RobotService} from "../../services/robot.service";

@Component({
    selector: 'app-show',
    standalone: true,
    imports: [
        CurrencyPipe, JsonPipe, NgIf, RouterLink
    ],
    templateUrl: './show.page.html',
    styles: []
})
export class ShowPage implements OnInit {
    robot: Robot | undefined;

    constructor(private robotSvc: RobotService, private route: ActivatedRoute, private router: Router) {
    }

    async ngOnInit() {
        const name = this.route.snapshot.paramMap.get('name') || '';
        this.robot = await this.robotSvc.one(name);
    }

    async remove() {
        await this.robotSvc.remove(this.robot?.name || '')
        await this.router.navigate(['list'])
    }

}
