import {Component, OnInit, ViewChild} from '@angular/core';
import {NgIf} from "@angular/common";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {FormGroup, FormsModule} from "@angular/forms";
import {Robot} from "../../domain/robot.domain";
import {RobotService} from "../../services/robot.service";

@Component({
    selector: 'app-edit',
    standalone: true,
    imports: [FormsModule, NgIf, RouterLink],
    templateUrl: './edit.page.html',
    styles: []
})
export class EditPage implements OnInit {
    robot: Robot | undefined;
    @ViewChild('form') form: FormGroup | undefined;

    constructor(private robotSvc: RobotService, private route: ActivatedRoute, private router: Router) {
    }

    async ngOnInit() {
        const name = this.route.snapshot.paramMap.get('name') || '';
        this.robot = await this.robotSvc.one(name);
    }

    async save() {
        if (this.robot) {
            this.robot = await this.robotSvc.update(this.robot);
        }
    }

}
