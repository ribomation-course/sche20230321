import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Subject, firstValueFrom} from "rxjs";
import {Robot} from "../domain/robot.domain";

export interface Pagination {
    items: Robot[];
    page: number;
}

export interface Summary {
    count: number;
    total: number;
    zero: number;
}

@Injectable({providedIn: 'root'})
export class RobotService {
    readonly url = 'http://localhost:8080/api/v1/robo';
    readonly channel = new Subject<Pagination>();

    constructor(private http: HttpClient) {
    }

    channel$() {
        return this.channel;
    }

    summary(): Promise<Summary> {
        return firstValueFrom(this.http.get<Summary>(`${this.url}/summary`));
    }

    search(phrase: string) {
        this.http.get<Robot[]>(`${this.url}/search?phrase=${phrase}`)
            .subscribe((items: Robot[]) => {
                this.channel.next({items, page: -1});
            });
    }

    one(name: string): Promise<Robot> {
        return firstValueFrom(this.http.get<Robot>(`${this.url}/${name}`));
    }

    list(): Promise<Pagination> {
        return this.first();
    }

    private paginate(action: string, page: number = 1): Promise<Pagination> {
        return firstValueFrom(this.http.get<Pagination>(`${this.url}/paginate/${action}/${page}`));
    }

    first(): Promise<Pagination> {
        return this.paginate('first');
    }

    last(): Promise<Pagination> {
        return this.paginate('last');
    }

    next(page: number): Promise<Pagination> {
        return this.paginate('next', page);
    }

    prev(page: number): Promise<Pagination> {
        return this.paginate('prev', page);
    }

    remove(name: string): Promise<void> {
        return firstValueFrom(this.http.delete<void>(`${this.url}/${name}`));
    }

    update(robot: Robot): Promise<Robot> {
        const url = `${this.url}/${robot.name}`;
        return firstValueFrom(this.http.put<Robot>(url, {
            price: robot.price,
            count: robot.count,
            city: robot.city,
            country: robot.country,
        } as any));
    }

    create(robot: Robot): Promise<Robot> {
        return firstValueFrom(this.http.post<Robot>(this.url, robot));
    }

}
