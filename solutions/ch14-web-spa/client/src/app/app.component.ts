import {Component} from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {NavbarWidget} from "./widgets/navbar/navbar.widget";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: [''],
    standalone: true,
    imports: [
        RouterOutlet,
        NavbarWidget
    ]
})
export class AppComponent {}
