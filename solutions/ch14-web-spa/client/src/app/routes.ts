import {Route} from "@angular/router";

export interface Nav {
    text: string;
    icon: string;
    uri?: string;
}

export const routes: Route[] = [
    {
        path: 'home',
        loadComponent: () => import('./pages/home/home.page').then(c => c.HomePage),
        data: {
            nav: {
                text: 'Home', icon: 'bi bi-house-door-fill', uri: '/home'
            } as Nav,
            title: 'Robo Home'
        }
    },
    {
        path: 'list',
        loadComponent: () => import('./pages/list/list.page').then(c => c.ListPage),
        data: {
            nav: {
                text: 'List', icon: 'bi-card-list', uri: '/list'
            } as Nav,
            title: 'Robo Listing'
        }
    },
    {
        path: 'summary',
        loadComponent: () => import('./pages/summary/summary.page').then(c => c.SummaryPage),
        data: {
            nav: {
                text: 'Summary', icon: 'bi-file-text', uri: '/summary'
            } as Nav,
            title: 'Robo Summary'
        }
    },
    {
        path: 'show/:name',
        loadComponent: () => import('./pages/show/show.page').then(c => c.ShowPage),
        data: {
            title: 'Robo Detail'
        }
    },
    {
        path: 'edit/:name',
        loadComponent: () => import('./pages/edit/edit.page').then(c => c.EditPage),
        data: {
            title: 'Robo Edit'
        }
    },
    {
        path: 'create',
        loadComponent: () => import('./pages/create/create.page').then(c => c.CreatePage),
        data: {
            nav: {
                text: 'Create', icon: 'bi-plus-square', uri: '/create'
            } as Nav,
            title: 'Robo Create'
        }
    },
    {
        path: '', redirectTo: '/home', pathMatch: 'full'
    }
];
