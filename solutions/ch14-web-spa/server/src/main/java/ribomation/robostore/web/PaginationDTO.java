package ribomation.robostore.web;

import ribomation.robostore.domain.RoboProduct;

import java.util.List;

public record PaginationDTO(Integer page, List<RoboProduct> items) { }
