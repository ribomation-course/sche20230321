package ribomation.robostore.web;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.jpa.RoboJpaDao;

@Controller
@RequestMapping("/robo")
public class RoboController {
    private final RoboJpaDao dao;

    public RoboController(RoboJpaDao dao) {
        this.dao = dao;
    }

    @GetMapping("list")
    public String list() {
        return "list";
    }


//    @GetMapping("summary")
//    public String summary(Model m) {
//        m.addAttribute("count", dao.count());
//        m.addAttribute("zero", dao.countByCountEquals(0));
//        m.addAttribute("total", dao.totalValue());
//        return "summary";
//    }

    @GetMapping("search")
    public String search(Model m, @RequestParam String phrase) {
        m.addAttribute("list", dao.findFirst5ByNameLikeIgnoreCase('%' + phrase + '%'));
        return "list";
    }

    @GetMapping("show/{name}")
    public String show(Model m, @PathVariable String name) {
        m.addAttribute("obj", dao.findById(name).orElseThrow(() -> new NotFound(name)));
        return "show";
    }

//    @GetMapping("remove/{name}")
//    public String remove(Model m, @PathVariable String name) {
//        dao.deleteById(name);
//        return "redirect:/robo/list";
//    }

    @GetMapping("edit/{name}")
    public String edit(Model m, @PathVariable String name) {
        m.addAttribute("obj", dao.findById(name).orElseThrow(() -> new NotFound(name)));
        return "edit";
    }

//    @PostMapping("update/{name}")
//    public String update(Model m, @PathVariable String name, UpdateDTO dto) {
//        var obj = dao.findById(name).orElseThrow(() -> new NotFound(name));
//        dao.save(dto.patch(obj));
//        return "redirect:/robo/show/" + name;
//    }

//    @GetMapping("create")
//    public String create() {
//        return "create";
//    }

//    @PostMapping("save")
//    public String save(CreateDTO dto) {
//        var name = dto.name;
//        dao.save(dto.toRobo());
//        return "redirect:/robo/show/" + name;
//    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String error(Model m, NotFound x) {
        m.addAttribute("name", x.name);
        return "error";
    }

}
