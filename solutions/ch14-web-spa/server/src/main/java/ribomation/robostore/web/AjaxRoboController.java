package ribomation.robostore.web;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ribomation.robostore.domain.RoboProduct;
import ribomation.robostore.jpa.RoboJpaDao;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/robo")
@CrossOrigin
public class AjaxRoboController {
    private final RoboJpaDao dao;

    public AjaxRoboController(RoboJpaDao dao) {
        this.dao = dao;
    }

    @GetMapping
    public List<RoboProduct> list() {
        return dao.findAll(Pageable.ofSize(12)).toList();
    }

    @GetMapping("/{name}")
    public RoboProduct one(@PathVariable String name) {
        return dao.findById(name).orElseThrow(() -> new NotFound(name));
    }

    @GetMapping("search")
    public List<RoboProduct> search(@RequestParam String phrase) {
        return dao.findFirst5ByNameLikeIgnoreCase('%' + phrase + '%');
    }

    @GetMapping("summary")
    public Map<String, Number> summary() {
        return Map.of(
                "count", dao.count(),
                "zero", dao.countByCountEquals(0),
                "total", dao.totalValue()
        );
    }

    @GetMapping({"paginate/{action}", "paginate/{action}/{page}"})
    public PaginationDTO paginate(@PathVariable PageAction action, @PathVariable(required = false) Integer page) {
        var nav = Pagination.of(page, dao.count()).paginate(action);
        page = nav.getPageNumber() + 1;
        var items = dao.findAll(nav).toList();
        return new PaginationDTO(page, items);
    }

    @DeleteMapping("/{name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String name) {
        dao.deleteById(name);
    }

    @PutMapping("/{name}")
    public RoboProduct update(@PathVariable String name, @RequestBody UpdateDTO dto) {
        System.out.printf("[update] name=%s, dto=%s%n", name, dto);
        var obj = dao.findById(name).orElseThrow(() -> new NotFound(name));
        dao.save(dto.patch(obj));
        return dao.findById(name).orElseThrow();
    }

    @PostMapping
    public RoboProduct save(@RequestBody CreateDTO dto) {
        var name = dto.name;
        dao.save(dto.toRobo());
        return dao.findById(name).orElseThrow(() -> new NotFound(name));
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Map<String, String> error(NotFound x) {
        return Map.of("name", x.name);
    }
}
