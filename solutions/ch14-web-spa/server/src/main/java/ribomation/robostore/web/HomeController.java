package ribomation.robostore.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ribomation.robostore.AppInfo;

import java.util.Map;

@RestController
@RequestMapping("/")
public class HomeController {
    private final AppInfo app;

    public HomeController(AppInfo app) {
        this.app = app;
    }

    @GetMapping
    public Map<String, String> index() {
        return Map.of(
                "app", app.getName(),
                "version", app.getVersion(),
                "author", app.getAuthor()
        );
    }

}
