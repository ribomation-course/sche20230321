package ribomation.robostore;

import ribomation.robostore.cli.RoboCommands;

public class Runner {
    private final RoboCommands cmd;

    public Runner(RoboCommands cmd) {
        this.cmd = cmd;
    }

    public void run(String[] args) {
        if (args.length == 0) {
            cmd.list();
            cmd.summary();
        } else {
            switch (args[0]) {
                case "summary" -> cmd.summary();
                case "list" -> cmd.list();
                case "first" -> cmd.listFirst();
                case "last" -> cmd.listLast();
                case "next" -> cmd.listNext(Integer.valueOf(args[1]));
                case "prev" -> cmd.listPrev(Integer.valueOf(args[1]));

                case "one" -> cmd.one(args[1]);
                case "delete" -> cmd.delete(args[1]);
                case "search" -> cmd.search(args[1]);

                case "update" ->
                        cmd.update(args[1], args[2], args[3], Float.valueOf(args[4]), Integer.valueOf(args[5]));
                case "create" ->
                        cmd.create(args[1], args[2], args[3], args[4], Float.valueOf(args[5]), Integer.valueOf(args[6]));
            }
        }
    }
}
