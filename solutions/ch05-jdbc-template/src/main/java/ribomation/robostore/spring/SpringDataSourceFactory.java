package ribomation.robostore.spring;

import org.h2.jdbcx.JdbcConnectionPool;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

@Service
public class SpringDataSourceFactory {
    private final String url;
    private final String usr;
    private final String pwd;

    public SpringDataSourceFactory() {
        try {
            var p = new Properties();
            p.load(getClass().getResourceAsStream("/db.properties"));
            url = p.getProperty("jdbc.url");
            usr = p.getProperty("jdbc.username");
            pwd = p.getProperty("jdbc.password");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public DataSource create() {
        return JdbcConnectionPool.create(url, usr, pwd);
    }
}
