package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.robostore.cli.RoboCommands;
import ribomation.robostore.jdbc.BeanPreparator;

import javax.sql.DataSource;

public class XmlApplicationObjects extends ApplicationObjects {
    private final ApplicationContext ctx;

    public XmlApplicationObjects() {
        ctx = new ClassPathXmlApplicationContext("/beans.xml");
    }

    @Override
    public BeanPreparator getBeanPreparator() {
        return ctx.getBean(BeanPreparator.class);
    }

    @Override
    public DataSource getDataSource() {
        return ctx.getBean("ds", DataSource.class);
    }

    @Override
    public RoboProductDAO getDao() {
        return ctx.getBean(RoboProductDAO.class);
    }

    @Override
    public RoboCommands getCommands() {
        return ctx.getBean(RoboCommands.class);
    }
}
