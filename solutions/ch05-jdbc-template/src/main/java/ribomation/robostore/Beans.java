package ribomation.robostore;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import ribomation.robostore.cli.RoboCommands;
import ribomation.robostore.jdbc.BeanPreparator;
import ribomation.robostore.jdbc.JdbcRoboProductDAO;

import javax.sql.DataSource;

@Configuration
public class Beans {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propsConfig() {
        var cfg = new PropertySourcesPlaceholderConfigurer();
        cfg.setLocation(new ClassPathResource("/db.properties"));
        return cfg;
    }

    @Bean
    public DataSource ds(
            @Value("${jdbc.url}") String url,
            @Value("${jdbc.username}") String usr,
            @Value("${jdbc.password}") String pwd
    ) {
        var h2 = new H2DataSourceFactory(url, usr, pwd);
        return h2.create();
    }

    @Bean
    public BeanPreparator preparator() {
        return new BeanPreparator();
    }

    @Bean
    public RoboProductDAO dao(DataSource ds, BeanPreparator prep) {
        return new JdbcRoboProductDAO(ds, prep);
    }

    @Bean
    public RoboCommands roboCommands(RoboProductDAO dao) {
        return new RoboCommands(dao);
    }

}
