package ribomation.robostore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ribomation.robostore.cli.RoboCommands;

public class XmlApplicationObjects extends ApplicationObjects {
    private final ApplicationContext ctx;

    public XmlApplicationObjects() {
        ctx = new ClassPathXmlApplicationContext("/beans.xml");
    }

    @Override
    public RoboCommands getCommands() {
        return ctx.getBean(RoboCommands.class);
    }
}
