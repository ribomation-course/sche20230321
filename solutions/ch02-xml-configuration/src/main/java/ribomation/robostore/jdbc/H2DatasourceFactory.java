package ribomation.robostore.jdbc;

import org.h2.jdbcx.JdbcConnectionPool;

import javax.sql.DataSource;

public class H2DatasourceFactory {
    String url, usr, pwd;

    public H2DatasourceFactory(String url, String usr, String pwd) {
        this.url = url;
        this.usr = usr;
        this.pwd = pwd;
    }

    public DataSource create() {
        return JdbcConnectionPool.create(url, usr, pwd);
    }
}
