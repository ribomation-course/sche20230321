package ribomation.robostore.web;

public enum PageAction {
    first, prev, next, last
}
