package ribomation.robostore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobostoreWeb {
    public static void main(String[] args) {
        SpringApplication.run(RobostoreWeb.class, args);
    }
}
