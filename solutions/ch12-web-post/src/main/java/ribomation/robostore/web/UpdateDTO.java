package ribomation.robostore.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ribomation.robostore.domain.RoboProduct;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateDTO {
    Float price;
    Integer count;
    String city;
    String country;

    public RoboProduct patch(RoboProduct r) {
        if (price != null && price > 0) r.setPrice(price);
        if (count != null && count >= 0) r.setCount(count);
        if (city != null && !city.isBlank()) r.setCity(city);
        if (country != null && !country.isBlank()) r.setCountry(country);
        return r;
    }
}
