package ribomation.robostore.web;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class Pagination extends PageRequest {
    private final int rowCount;

    protected Pagination(int page, int pageSize, int rowCount) {
        super(page, pageSize, Sort.unsorted());
        this.rowCount = rowCount;
    }

    public static Pagination of(Integer page, int pageSize, long rowCount) {
        return new Pagination(page != null ? page - 1 : 0, pageSize, (int) rowCount);
    }

    public static Pagination of(Integer page, long rowCount) {
        return Pagination.of(page, 10, rowCount);
    }

    public PageRequest last() {
        var pageCount = rowCount / getPageSize();
        return withPage(pageCount - 1);
    }

    @Override
    public PageRequest next() {
        var nav = super.next();
        var pageCount = rowCount / getPageSize();
        if (nav.getPageNumber() < pageCount) return nav;
        return last();
    }

    public PageRequest paginate(PageAction action) {
        if (action == null) action = PageAction.first;
        return switch (action) {
            case first -> first();
            case prev -> previous();
            case next -> next();
            case last -> last();
        };
    }
}

