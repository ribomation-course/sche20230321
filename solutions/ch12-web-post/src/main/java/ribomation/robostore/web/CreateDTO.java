package ribomation.robostore.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ribomation.robostore.domain.RoboProduct;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateDTO {
    String name;
    String description;
    String city;
    String country;
    Float price;
    Integer count;

    public RoboProduct toRobo() {
        return RoboProduct.builder()
                .name(name)
                .description(description)
                .city(city)
                .country(country)
                .price(price)
                .count(count)
                .build()
                .complete();
    }
}
