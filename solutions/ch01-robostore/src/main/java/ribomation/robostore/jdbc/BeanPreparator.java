package ribomation.robostore.jdbc;

import ribomation.robostore.domain.RoboProduct;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BeanPreparator {
    private RoboProduct robo;

    public void setRobo(RoboProduct robo) {
        this.robo = robo;
    }

    public void setValues(PreparedStatement ps) throws SQLException {
        if (robo == null) {
            throw new IllegalArgumentException("Missing robo object");
        }
        setValues(ps, robo);
        robo = null;
    }

    public void setValues(PreparedStatement ps, RoboProduct robo) throws SQLException {
        try {
            int idx = 1;
            for (var name : RoboProduct.names) {
                var field = RoboProduct.class.getDeclaredField(name);
                field.setAccessible(true);
                var value = field.get(robo);
                var type = field.getType();
                if (type.equals(String.class)) {
                    ps.setString(idx, value.toString());
                } else if (type.equals(Integer.class) || type.equals(int.class)) {
                    ps.setInt(idx, (Integer) value);
                } else if (type.equals(Float.class) || type.equals(float.class)) {
                    ps.setFloat(idx, (Float) value);
                }
                ++idx;
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public RoboProduct mapRow(ResultSet rs) {
        try {
            return RoboProduct.builder()
                    .name(rs.getString("name"))
                    .price(rs.getFloat("price"))
                    .count(rs.getInt("count"))
                    .description(rs.getString("description"))
                    .uid(rs.getString("uid"))
                    .city(rs.getString("city"))
                    .country(rs.getString("country"))
                    .icon(rs.getString("icon"))
                    .image(rs.getString("image"))
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
