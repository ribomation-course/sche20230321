package ribomation.robostore;

public class RobostoreCLI {
    public static void main(String[] args) {
        var app = new RobostoreCLI();
        app.run(args, new ApplicationObjects());
    }

    void run(String[] args, ApplicationObjects app) {
        if (args.length == 0) {
            System.err.println("usage: <cmd> [<arg>]");
            System.exit(1);
        }

        var cmd = app.getCommands();
        var action = args[0];
        switch (action) {
            case "list" -> cmd.list();
            case "summary" -> cmd.summary();
            case "one" -> cmd.one(args[1]);
            case "delete" -> cmd.delete(args[1]);
        }
    }

}
