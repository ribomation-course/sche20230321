package ribomation.robostore;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ribomation.robostore.cli.RoboCommands;

//@Configuration
//@ComponentScan(basePackages = {"ribomation.robostore.spring"})
public class AnnotationApplicationObjects extends ApplicationObjects {
    private final AnnotationConfigApplicationContext ctx;

    public AnnotationApplicationObjects() {
        ctx = new AnnotationConfigApplicationContext();
        ctx.scan("ribomation.robostore.spring");
        ctx.refresh();
    }

    @Override
    public RoboCommands getCommands() {
        return ctx.getBean(RoboCommands.class);
    }

}
