package ribomation.robostore.jdbc;

import ribomation.robostore.RoboProductDAO;
import ribomation.robostore.domain.RoboProduct;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

public class JdbcRoboProductDAO implements RoboProductDAO {
    private static final String TBL = "ROBO_STORE";
    private final DataSource ds;
    private final BeanPreparator beanPreparator;

    public JdbcRoboProductDAO(DataSource dataSource, BeanPreparator beanPreparator) {
        this.ds = dataSource;
        this.beanPreparator = beanPreparator;
    }

    @Override
    public float totalValue() {
        var sql = format("select sum(t.price * t.count) from %s t", TBL);
        try (var con = ds.getConnection()) {
            try (var stmt = con.createStatement()) {
                try (var rs = stmt.executeQuery(sql)) {
                    if (rs.next()) {
                        return rs.getFloat(1);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    @Override
    public int count() {
        var sql = format("select count(*) from %s", TBL);
        try (var con = ds.getConnection()) {
            try (var stmt = con.createStatement()) {
                try (var rs = stmt.executeQuery(sql)) {
                    if (rs.next()) {
                        return rs.getInt(1);
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }

    @Override
    public List<RoboProduct> list(int offset, int rows) {
        var sql = format("select * from %s offset %d rows fetch first %d rows only", TBL, offset, rows);
        return doList(sql);
    }

    @Override
    public List<RoboProduct> search(String phrase) {
        var sql = format("select * from %s where name ilike '%s'", TBL, '%' + phrase + '%');
        return doList(sql);
    }

    @Override
    public List<RoboProduct> outOfStock() {
        var sql = format("select * from %s where count = 0", TBL);
        return doList(sql);
    }

    private List<RoboProduct> doList(String sql) {
        var lst = new ArrayList<RoboProduct>();
        try (var con = ds.getConnection()) {
            try (var stmt = con.createStatement()) {
                try (var rs = stmt.executeQuery(sql)) {
                    while (rs.next()) lst.add(beanPreparator.mapRow(rs));
                    return lst;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<RoboProduct> byName(String name) {
        var sql = format("select * from %s where name = '%s'", TBL, name);
        try (var con = ds.getConnection()) {
            try (var stmt = con.createStatement()) {
                try (var rs = stmt.executeQuery(sql)) {
                    if (rs.next()) return Optional.of(beanPreparator.mapRow(rs));
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean exists(String name) {
        var sql = format("select * from %s where name = '%s'", TBL, name);
        try (var con = ds.getConnection()) {
            try (var stmt = con.createStatement()) {
                try (var rs = stmt.executeQuery(sql)) {
                    return rs.next();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insert(RoboProduct robo) {
        var sql = format("insert into %s (%s) values (%s)", TBL, columns(), marks());
        try (var con = ds.getConnection()) {
            try (var ps = con.prepareStatement(sql)) {
                beanPreparator.setValues(ps, robo);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insert(List<RoboProduct> list) {
        var sql = format("insert into %s (%s) values (%s)", TBL, columns(), marks());
        try (var con = ds.getConnection()) {
            try (var ps = con.prepareStatement(sql)) {
                for (var robo : list) {
                    beanPreparator.setValues(ps, robo);
                    ps.executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(String name, RoboProduct robo) {
        var sql = format("update %s set price=?, count=?, city=?, country=? where name = ?", TBL);
        try (var con = ds.getConnection()) {
            try (var ps = con.prepareStatement(sql)) {
                beanPreparator.setValues(ps, robo);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(String name) {
        var sql = format("delete from %s where name = '%s'", TBL, name);
        try (var con = ds.getConnection()) {
            try (var s = con.createStatement()) {
                s.executeUpdate(sql);
            }
        }catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private String columns() {
        return String.join(",", RoboProduct.names);
    }

    private String marks() {
        return Stream.of(RoboProduct.names).map(__ -> "?").collect(Collectors.joining(","));
    }

}
