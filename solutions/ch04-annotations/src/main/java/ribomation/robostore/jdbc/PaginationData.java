package ribomation.robostore.jdbc;

public class PaginationData {
    private final int rowsPerPage;
    private final int pagesTotal;

    public PaginationData(int rowsPerPage, int rowsTotal) {
        this.rowsPerPage = rowsPerPage;
        this.pagesTotal = rowsTotal / rowsPerPage;
    }

    public PaginationData(int rowsTotal) {
        this(10, rowsTotal);
    }

    public int pageSize() {
        return rowsPerPage;
    }

    public int pageTo(PaginationAction action) {
        return pageTo(action, null);
    }

    public int pageTo(PaginationAction action, Integer page) {
        if (page == null) page = 1;
        return clamp(1, pagesTotal, switch (action) {
            case first -> 1;
            case prev -> page - 1;
            case next -> page + 1;
            case last -> pagesTotal;
        });
    }

    public int offset(int page) {
        return (page - 1) * pageSize();
    }

    private int clamp(int lb, int ub, int val) {
        if (val < lb) return lb;
        if (val > ub) return ub;
        return val;
    }
}
