package ribomation.robostore.spring;

import org.springframework.stereotype.Repository;
import ribomation.robostore.jdbc.BeanPreparator;
import ribomation.robostore.jdbc.JdbcRoboProductDAO;

@Repository
public class SpringDAO extends JdbcRoboProductDAO {
    public SpringDAO(SpringDataSourceFactory factory, BeanPreparator beanPreparator) {
        super(factory.create(), beanPreparator);
    }
}
