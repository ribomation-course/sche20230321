package ribomation.robostore;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
@ConfigurationProperties(prefix = "robostore")
public class AppInfo {
    String name;
    String version;
    String author;

    @Override
    public String toString() {
        return new StringJoiner(", ", AppInfo.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("version='" + version + "'")
                .add("author='" + author + "'")
                .toString();
    }
    public void setName(String name) {this.name = name;}
    public void setVersion(String version) {this.version = version;}
    public void setAuthor(String author) {this.author = author;}
}
