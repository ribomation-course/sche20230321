package ribomation.robostore.cli;

import ribomation.robostore.domain.RoboProductDAO;
import ribomation.robostore.domain.RoboProduct;
import ribomation.robostore.jdbc.PaginationAction;
import ribomation.robostore.jdbc.PaginationData;

import java.io.PrintWriter;

public class RoboCommands {
    private final RoboProductDAO dao;
    private final PrintWriter out;

    public RoboCommands(RoboProductDAO dao) {
        this.dao = dao;
        this.out = new PrintWriter(System.out, true);
    }

    public void summary() {
        out.printf("# robo        : %d%n", dao.count());
        out.printf("total value   : %.2f kr%n", dao.totalValue());
        out.printf("# out of stock: %d%n", dao.outOfStock().size());
    }

    private void paginate(PaginationAction action, Integer page) {
        var data = new PaginationData(dao.count());
        page = data.pageTo(PaginationAction.next, page);
        dao.list(data.offset(page), data.pageSize()).forEach(out::println);
        out.printf("Page: %d%n", page);
    }

    public void list()                 {dao.list().forEach(out::println);}
    public void listFirst()            {paginate(PaginationAction.first, null);}
    public void listPrev(Integer page) {paginate(PaginationAction.prev, page);}
    public void listNext(Integer page) {paginate(PaginationAction.next, page);}
    public void listLast()             {paginate(PaginationAction.last, null);}
    public void search(String phrase)  {dao.search(phrase).stream().limit(15).forEach(out::println);}
    public void one(String name)       {dao.byName(name).ifPresent(out::println);}

    public void delete(String name) {
        if (dao.exists(name)) {
            dao.delete(name);
        } else {
            out.printf("Not found: %s%n", name);
        }
    }

    public void update(String name, String city, String country, Float price, Integer count) {
        if (dao.exists(name)) {
            var robo = dao.byName(name).get();
            var delta = RoboProduct.builder();

            delta.city(city != null ? city : robo.getCity());
            delta.country(country != null ? country : robo.getCountry());
            delta.price(price != null ? price : robo.getPrice());
            delta.count(count != null ? count : robo.getCount());

            dao.update(name, delta.build());
            dao.byName(name).ifPresent(out::println);
        } else {
            out.printf("Not found: %s%n", name);
        }
    }

    public void create(String name, String description, String city, String country, Float price, Integer count) {
        if (dao.exists(name)) {
            out.printf("Robo '%s' already exists%n", name);
            return;
        }

        var robo = RoboProduct.builder();
        robo.name(name);
        if (description != null) robo.description(description);
        if (city != null) robo.city(city);
        if (country != null) robo.country(country);
        if (price != null) robo.price(price);
        if (count != null) robo.count(count);

        dao.insert(robo.build().complete());
        dao.byName(name).ifPresent(out::println);
    }
}
