package ribomation.robostore;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

@Component
@ConfigurationProperties(prefix = "robostore")
@Data
public class AppInfo {
    String name;
    String version;
    String author;
}
