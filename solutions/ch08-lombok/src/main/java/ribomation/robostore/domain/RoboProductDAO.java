package ribomation.robostore.domain;

import ribomation.robostore.domain.RoboProduct;

import java.util.List;
import java.util.Optional;

public interface RoboProductDAO {
    int count();
    float totalValue();

    List<RoboProduct> list(int offset, int rows);
    default List<RoboProduct> list() {return list(0, 10);}
    List<RoboProduct> search(String phrase);
    List<RoboProduct> outOfStock();

    boolean exists(String name);
    Optional<RoboProduct> byName(String name);

    void insert(RoboProduct robo);
    void insert(List<RoboProduct> list);
    void update(String name, RoboProduct robo);
    void delete(String name);
}
