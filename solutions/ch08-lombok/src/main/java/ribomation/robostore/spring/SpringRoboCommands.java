package ribomation.robostore.spring;

import org.springframework.stereotype.Service;
import ribomation.robostore.domain.RoboProductDAO;
import ribomation.robostore.cli.RoboCommands;

@Service
public class SpringRoboCommands extends RoboCommands {
    public SpringRoboCommands(RoboProductDAO dao) {
        super(dao);
    }
}
