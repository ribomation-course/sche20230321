package ribomation.robostore.cli;

import org.jline.terminal.Terminal;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ribomation.robostore.AppInfo;
import ribomation.robostore.domain.RoboProduct;
import ribomation.robostore.domain.RoboProductDAO;
import ribomation.robostore.jdbc.PaginationAction;
import ribomation.robostore.jdbc.PaginationData;

import java.util.Properties;

@Deprecated
//@ShellComponent
public class RoboCommands {
    private final RoboProductDAO dao;
    private final Terminal terminal;

    public RoboCommands(RoboProductDAO dao, Terminal terminal, AppInfo app, Properties buildInfo) {
        this.dao = dao;
        this.terminal = terminal;

        var out = terminal.writer();
        out.println(app);
        out.println(buildInfo);
        out.println("----");
    }

    @ShellMethod(value = "Prints a summary of the DB content", group = "Misc.")
    public void summary() {
        var out = terminal.writer();
        out.printf("# products    : %d%n", dao.count());
        out.printf("# out of stock: %d%n", dao.outOfStock().size());
        out.printf("total value   : %.2f kr%n", dao.totalValue());
    }

    private void paginate(PaginationAction action, Integer page) {
        var data = new PaginationData(dao.count());
        page = data.pageTo(action, page);

        var out = terminal.writer();
        dao.list(data.offset(page), data.pageSize()).forEach(out::println);
        out.printf("Page: %d%n", page);
    }

    @ShellMethod(value = "List the first 10 rows", group = "list")
    public void list()                 {dao.list().forEach(terminal.writer()::println);}

    @ShellMethod(value = "List the first N rows", group = "list")
    public void listFirst()            {paginate(PaginationAction.first, null);}

    @ShellMethod(value = "List the previous N rows", group = "list")
    public void listPrev(Integer page) {paginate(PaginationAction.prev, page);}

    @ShellMethod(value = "List the next N rows", group = "list")
    public void listNext(Integer page) {paginate(PaginationAction.next, page);}

    @ShellMethod(value = "List the last N rows", group = "list")
    public void listLast()             {paginate(PaginationAction.last, null);}

    @ShellMethod(value = "Show robo(s) with partial name", group = "read")
    public void search(String phrase)  {dao.search(phrase).stream().limit(15).forEach(terminal.writer()::println);}

    @ShellMethod(value = "Show robo with the name", group = "read")
    public void one(String name)       {dao.byName(name).ifPresent(terminal.writer()::println);}

    @ShellMethod(value = "Delete robo with name", group = "crud")
    public void delete(String name) {
        if (dao.exists(name)) {
            dao.delete(name);
        } else {
            terminal.writer().printf("Not found: %s%n", name);
        }
    }

    @ShellMethod(value = "Update robo with name", group = "crud")
    public void update(String name,
                       @ShellOption(defaultValue = ShellOption.NULL) String city,
                       @ShellOption(defaultValue = ShellOption.NULL) String country,
                       @ShellOption(defaultValue = ShellOption.NULL) Float price,
                       @ShellOption(defaultValue = ShellOption.NULL) Integer count) {
        if (dao.exists(name)) {
            var delta = RoboProduct.builder();

            var robo = dao.byName(name).orElseThrow();
            delta.city(city != null ? city : robo.getCity());
            delta.country(country != null ? country : robo.getCountry());
            delta.price(price != null ? price : robo.getPrice());
            delta.count(count != null ? count : robo.getCount());

            dao.update(name, delta.build());
            dao.byName(name).ifPresent(terminal.writer()::println);
        } else {
            terminal.writer().printf("Not found: %s%n", name);
        }
    }

    @ShellMethod(value = "Create new robo", group = "crud")
    public void create(String name,
                       @ShellOption(defaultValue = ShellOption.NULL) String description,
                       @ShellOption(defaultValue = ShellOption.NULL) String city,
                       @ShellOption(defaultValue = ShellOption.NULL) String country,
                       @ShellOption(defaultValue = ShellOption.NULL) Float price,
                       @ShellOption(defaultValue = ShellOption.NULL) Integer count) {
        if (dao.exists(name)) {
            terminal.writer().printf("Robo '%s' already exists%n", name);
            return;
        }

        var robo = RoboProduct.builder();
        robo.name(name);
        if (description != null) robo.description(description);
        if (city != null) robo.city(city);
        if (country != null) robo.country(country);
        if (price != null) robo.price(price);
        if (count != null) robo.count(count);

        dao.insert(robo.build().complete());
        dao.byName(name).ifPresent(terminal.writer()::println);
    }
}
