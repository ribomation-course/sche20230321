package ribomation.robostore.cli;

import org.jline.terminal.Terminal;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ribomation.robostore.jpa.RoboJpaDao;

@ShellComponent
public class RoboCommandsJpa {
    private final RoboJpaDao dao;
    private final Terminal terminal;

    public RoboCommandsJpa(RoboJpaDao dao, Terminal terminal) {
        this.dao = dao;
        this.terminal = terminal;
    }

    @ShellMethod(value = "Prints a summary of the DB content", group = "Misc.")
    public void summary() {
        var out = terminal.writer();
        out.printf("# products    : %d%n", dao.count());
        out.printf("# out of stock: %d%n", dao.countByCount(0));
        out.printf("total value   : %.2f kr%n", dao.totalValue());
    }
}
