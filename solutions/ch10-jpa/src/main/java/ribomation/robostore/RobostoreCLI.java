package ribomation.robostore;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import ribomation.robostore.cli.RoboCommands;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SpringBootApplication
public class RobostoreCLI {
    public static void main(String[] args) {
        SpringApplication.run(RobostoreCLI.class, args);
    }

//    @Bean
//    public CommandLineRunner invokeCommand(ApplicationContext ctx, AppInfo app, Properties buildInfo) {
//        return args -> {
//            System.out.println(app);
//            System.out.println(buildInfo);
//            System.out.println("----");
//
//            var cmd = ctx.getBean(RoboCommands.class);
//            var runner = new Runner(cmd);
//            runner.run(args);
//        };
//    }

    @Bean
    public Properties buildInfo() throws IOException {
        var props = new Properties();
        var is = getClass().getResourceAsStream("/META-INF/build-info.properties");
        if (is != null) props.load(is);
        return props;
    }

}
