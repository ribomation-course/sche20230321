package ribomation.robostore.spring;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ribomation.robostore.domain.RoboProductDAO;
import ribomation.robostore.domain.RoboProduct;
import ribomation.robostore.jdbc.BeanPreparator;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

@Deprecated
//@Repository
public class JdbcTemplateDAO implements RoboProductDAO {
    private static final String TBL = "ROBO_STORE";
    private final JdbcTemplate jdbc;
    private final BeanPreparator mapper;

    public JdbcTemplateDAO(DataSource ds, BeanPreparator mapper) {
        this.jdbc = new JdbcTemplate(ds);
        this.mapper = mapper;
    }

    @Override
    public int count() {
        var sql = format("select count(*) from %s", TBL);
        return jdbc.queryForObject(sql, Integer.class);
    }

    @Override
    public float totalValue() {
        var sql = format("select sum(t.price * t.count) from %s t", TBL);
        return jdbc.queryForObject(sql, Float.class);
    }

    @Override
    public List<RoboProduct> outOfStock() {
        var sql = format("select * from %s where count = 0", TBL);
        return jdbc.query(sql, mapper);
    }

    @Override
    public List<RoboProduct> search(String phrase) {
        var sql = format("select * from %s where name ilike ?", TBL);
        return jdbc.query(sql, mapper, phrase);
    }

    @Override
    public List<RoboProduct> list(int offset, int rows) {
        var sql = format("select * from %s offset ? rows fetch first ? rows only", TBL);
        return jdbc.query(sql, mapper, offset, rows);
    }

    @Override
    public Optional<RoboProduct> byName(String name) {
        var sql = format("select * from %s where name = ?", TBL);
        try {
            var obj = jdbc.queryForObject(sql, mapper, name);
            return Optional.of(obj);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean exists(String name) {
        return byName(name).isPresent();
    }

    @Override
    public void insert(RoboProduct robo) {
        var sql = format("insert into %s (%s) values (%s)", TBL, columns(), marks());
        mapper.setRobo(robo);
        jdbc.update(sql, mapper);
    }

    @Override
    public void insert(List<RoboProduct> list) {
        var sql = format("insert into %s (%s) values (%s)", TBL, columns(), marks());
        jdbc.batchUpdate(sql, list, 10, mapper);
    }

    @Override
    public void update(String name, RoboProduct robo) {
        var sql = format("update %s set price=?, count=?, city=?, country=? where name = ?", TBL);
        jdbc.update(sql, robo.getPrice(), robo.getCount(), robo.getCity(), robo.getCountry(), name);
    }

    @Override
    public void delete(String name) {
        var sql = format("delete from %s where name = ?", TBL);
        jdbc.update(sql, name);
    }

    private String columns() {
        return String.join(",", RoboProduct.names);
    }

    private String marks() {
        return Stream.of(RoboProduct.names).map(__ -> "?").collect(Collectors.joining(","));
    }

}
