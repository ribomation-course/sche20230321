package ribomation.robostore.jdbc;

public enum PaginationAction {
    first, prev, next, last
}
