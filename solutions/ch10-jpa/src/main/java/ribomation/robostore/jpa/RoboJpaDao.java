package ribomation.robostore.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ribomation.robostore.domain.RoboProduct;

public interface RoboJpaDao extends JpaRepository<RoboProduct, String>
{
    int countByCount(int value);

    @Query("select sum(t.price * t.count) from RoboProduct t")
    float totalValue();
}
