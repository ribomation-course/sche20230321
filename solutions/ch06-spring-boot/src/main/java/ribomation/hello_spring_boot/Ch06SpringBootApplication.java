package ribomation.hello_spring_boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Ch06SpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(Ch06SpringBootApplication.class, args);
    }

    @Bean
    CommandLineRunner doit() {
        return args -> { System.out.println("Hello from Spring Boot"); };
    }
}
