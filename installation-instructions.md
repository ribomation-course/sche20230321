# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## Zoom Client
* [Zoom Desktop Client Download](https://us02web.zoom.us/download)
* [Using the Zoom Web Client](https://support.zoom.us/hc/en-us/articles/214629443-Getting-started-with-the-Zoom-web-client)
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## Java JDK
The decent version of Java JDK installed, appropriate for Spring, such as v19,
or at least v16.
* [Java JDK Download](https://jdk.java.net/19/)

## Access to Spring Initializr Web
Ability to configure and download a project ZIP from `https://start.spring.io/`

## IDE
A decent IDE, such as any of

* JetBrains IntelliJ IDEA
  - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
  - https://code.visualstudio.com/download
* Eclipse
  - https://www.eclipse.org/ide/
* Something else, you already are familiar with

## Build Tool
Need to have one of the build tools Maven or Gradle installed. However,
when generating a project via Spring Initializr, there is a wrapper shell
script to use.

* Gradle: `gradlew` / `gradle.bat`
* Maven: `mvnw` / `mvnw.cmd`

## GIT Client
* [Git Client Download](https://git-scm.com/downloads)

## Node.js & NPM/NPX
At the end of the course, there is an exercise involving a single-page app client.
If you want to perform these exercises, by building the client,
you need to have Node.js installed as well.

* [Node.js & NPM/NPX](https://nodejs.org/en/)

N.B., it's possible to complete the exercises without Node.js, by just copy the
pre-built files. However, it would not be as fun.

