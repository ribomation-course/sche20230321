package ribomation.nano_spring.api.impl;

import java.lang.reflect.Proxy;
import java.sql.Connection;

public class DaoFactory<DaoInterface, DomainClass>  {
    private final Class<DaoInterface> daoInterface;
    private final Class<DomainClass> domainClass;

    public DaoFactory(Class<DaoInterface> daoInterface, Class<DomainClass> domainClass) {
        this.daoInterface = daoInterface;
        this.domainClass  = domainClass;
    }

    public DaoInterface create(Connection connection) {
        var loader  = this.getClass().getClassLoader();
        var type    = new Class[]{daoInterface, AutoCloseable.class};
        var handler = new DaoHandler(domainClass, connection);
        return (DaoInterface) Proxy.newProxyInstance(loader, type, handler);
    }
}


