package ribomation.nano_spring.api.impl;

import ribomation.nano_spring.api.Column;
import ribomation.nano_spring.api.Default;
import ribomation.nano_spring.api.NotNull;
import ribomation.nano_spring.api.PrimaryKey;

import java.lang.reflect.Field;


public class ColumnDescriptor {
    private final String name;
    private final String type;
    private final boolean pk;
    private final boolean autoInc;
    private final boolean notNull;
    private final String defVal;
    private final Java2Jdbc mapper = new Java2Jdbc();

    public ColumnDescriptor(Field field) {
        name    = mkName(field);
        type    = mkType(field);
        pk      = mkPrimaryKey(field);
        autoInc = pk;
        notNull = mkNotNull(field);
        defVal  = mkDefault(field);
    }

    public String columnName() {
        return name;
    }

    public String columnType() {
        return String.join(" ",
                    type,
                    pk      ? "PRIMARY KEY" : "",
                    autoInc ? "AUTO_INCREMENT" : "",
                    notNull ? "NOT NULL" : "",
                    defVal != null ? "DEFAULT " + defVal : ""
                ).replaceAll("\\s+", " ");
    }

    private String mkName(Field field) {
        return field.getName().toLowerCase();
    }

    private String mkType(Field field) {
        Column column = field.getAnnotation(Column.class);
        return column.value().isBlank() ? mapper.toJdbc(field.getType()) : column.value();
    }

    private boolean mkPrimaryKey(Field field) {
        return field.getAnnotation(PrimaryKey.class) != null;
    }

    private boolean mkNotNull(Field field) {
        return field.getAnnotation(NotNull.class) != null;
    }

    private String mkDefault(Field field) {
        Default def = field.getAnnotation(Default.class);
        return def != null ? def.value() : null;
    }

}

