package ribomation.nano_spring.api.impl;

import java.util.*;

public class Java2Jdbc {
    private static final Map<Class, String> java2jdbc = new HashMap<>();

    static {//N.B., limited/incomplete mapping and tied to H2
        java2jdbc.put(Boolean.class, "BOOLEAN");
        java2jdbc.put(Date.class, "TIMESTAMP");
        java2jdbc.put(Double.class, "DOUBLE PRECISION");
        java2jdbc.put(Float.class, "REAL");
        java2jdbc.put(Integer.class, "INTEGER");
        java2jdbc.put(Long.class, "BIGINT");
        java2jdbc.put(String.class, "VARCHAR(64)");
        java2jdbc.put(boolean.class, "BOOLEAN");
        java2jdbc.put(double.class, "DOUBLE PRECISION");
        java2jdbc.put(float.class, "REAL");
        java2jdbc.put(int.class, "INTEGER");
        java2jdbc.put(long.class, "BIGINT");
    }

    /**
     * Returns the SQL type for a Java class
     * @param cls class
     * @return SQL type
     */
    public String toJdbc(Class cls) {
        if (java2jdbc.containsKey(cls)) return java2jdbc.get(cls);
        throw new IllegalArgumentException("unmapped java type: " + cls.getName());
    }

    /**
     * Returns the Java class for a SQL type
     * @param type SQL type
     * @return Java class
     */
    public Class fromJdbc(String type) {
        final String sqlType = type.toUpperCase();
        return java2jdbc.entrySet().stream()
                .filter(e -> e.getValue().startsWith(sqlType))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElseThrow();
    }

    public void add(Class javaType, String sqlType) {
        java2jdbc.put(javaType, sqlType.toUpperCase());
    }
    public List<Class> javaTypes() {
        return new ArrayList<>(java2jdbc.keySet());
    }
    public List<String> sqlTypes() {
        return new ArrayList<>(new TreeSet<>(java2jdbc.values()));
    }
}


