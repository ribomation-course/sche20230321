package ribomation.nano_spring.domain;

import ribomation.nano_spring.api.GenericDAO;
import java.util.List;
import java.util.Optional;

public interface InvoiceDAO extends GenericDAO<Invoice> {
    List<Invoice> findAllByCustomer(String name);
    List<Invoice> findAllByAmount(int value);
}

