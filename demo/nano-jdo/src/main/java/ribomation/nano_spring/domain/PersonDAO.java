package ribomation.nano_spring.domain;

import ribomation.nano_spring.api.GenericDAO;

import java.util.List;

public interface PersonDAO extends GenericDAO<Person> {
    List<Person> findAllByAge(int age);
}

