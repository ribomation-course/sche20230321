package ribomation.nano_spring.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class H2 {
    private static final String HOST        = "localhost";
    private static final int    PORT        = 9092;
    private static final String USERNAME    = "sa";
    private static final String PASSWORD    = "";
    private static final String JDBC_PREFIX = "jdbc:h2";

    public enum H2Types {
        memory("mem"), directory(""), socket("tcp");
        public final String type;

        H2Types(String type) {
            this.type = type;
        }
    }

    static String mkUrl(H2Types type, String database) {
        return switch (type) {
            case memory    -> String.format("%s:%s:%s", JDBC_PREFIX, type.type, database);
            case directory -> String.format("%s:%s/%s", JDBC_PREFIX, "db_files", database);
            case socket    -> String.format("%s:%s://%s:%d/%s", JDBC_PREFIX, type.type, HOST, PORT, database);
        };
    }

    public Connection open(H2Types type, String database) {
        try {
            var url = mkUrl(type, database);
            return DriverManager.getConnection(url, USERNAME, PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> tableNames(Connection c) {
        var names = new ArrayList<String>();
        try (var s = c.createStatement();
             var rs = s.executeQuery("SHOW TABLES"))
        {
            while (rs.next()) {
                names.add(rs.getString(1));
            }
            return names;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
