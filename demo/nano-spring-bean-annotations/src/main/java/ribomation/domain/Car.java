package ribomation.domain;

import ribomation.nano_spring.Bean;
import ribomation.nano_spring.Inject;

import java.util.Objects;
@Bean("amazon")
public class Car {
    private  String model;

    private @Inject Engine engine;

    public Car() {
        var ann = getClass().getAnnotation(Bean.class);
        if (!ann.value().isBlank()) {
            this.model = ann.value();
        }
    }

    public Car(String model, Engine engine) {
        this.model = model;
        this.engine = engine;
    }

    @Override
    public String toString() {
        return String.format("Car{%s, %s}", model, engine);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }

    public String getModel() {
        return model;
    }

    public Engine getEngine() {
        return engine;
    }
}
