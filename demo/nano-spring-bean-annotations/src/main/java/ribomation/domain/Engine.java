package ribomation.domain;

import ribomation.nano_spring.Bean;

import java.util.Objects;

@Bean("b18")
public class Engine {
    private String fuelType;

    public Engine() {
        this.fuelType = "petrol";
    }

    public Engine(String fuelType) {
        this.fuelType = fuelType;
    }

    @Override
    public String toString() {
        return String.format("Engine{%s}", fuelType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Engine engine = (Engine) o;
        return Objects.equals(fuelType, engine.fuelType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fuelType);
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }
}
