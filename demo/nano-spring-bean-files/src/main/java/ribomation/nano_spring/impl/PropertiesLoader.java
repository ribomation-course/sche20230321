package ribomation.nano_spring.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesLoader {
    private final BeanRepositoryImpl repo;

    public PropertiesLoader(BeanRepositoryImpl repo) {
        this.repo = repo;
    }

    public void load(InputStream is) {
        try {
            var beanDefinitions = new Properties();
            beanDefinitions.load(is);
            beanDefinitions.forEach((name, value) -> {
                var beanName  = name.toString();
                var items     = value.toString().split(";");
                var beanClass = items[0];
                var constructorArgs = Arrays.stream(items).skip(1).collect(Collectors.toList());
                repo.register(beanName, beanClass, constructorArgs);
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(String beanResource) {
        if (!beanResource.endsWith(".properties")) {
            throw new RuntimeException("Not a *.properties file: " + beanResource);
        }
        try (var in = this.getClass().getResourceAsStream(beanResource)) {
            if (in == null) {
                throw new RuntimeException("Cannot open bean file: " + beanResource);
            }
            load(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void load(Path beanFile) {
        if (!beanFile.getFileName().endsWith(".properties")) {
            throw new RuntimeException("Not a *.properties file: " + beanFile);
        }
        try (var in = Files.newInputStream(beanFile)) {
            load(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
