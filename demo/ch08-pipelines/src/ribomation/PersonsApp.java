package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PersonsApp {
    public static void main(String[] args) throws IOException {
        new PersonsApp().run();
    }

    private void run() throws IOException {
        var cnt = Files.lines(Path.of("data", "persons.csv"))
                .skip(1)
                .map(Person::fromCSV)
                .filter(Person::female)
                .filter(p -> 30 <= p.age() && p.age() <= 40)
                .filter(p -> p.post_code() < 20_000)
                .count();
        System.out.printf("Found %d persons%n", cnt);
    }
}
