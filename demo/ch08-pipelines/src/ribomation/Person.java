package ribomation;

public record Person(String name, boolean female, int age, int post_code) {
    static Person fromCSV(String csvLine) {
//        name;gender;age;post_code
//        Erik Pulsford;Male;44;27996
        var parts = csvLine.split(";");
        return new Person(parts[0],
                parts[1].equals("Female"),
                Integer.parseInt(parts[2]),
                Integer.parseInt(parts[3]));
    }
}
