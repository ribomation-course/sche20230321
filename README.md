# Spring Framework and Spring Boot, 4 course days
## 2023 March

# Links
* [Installation Instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/jvm/spring-framework/)


# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed,
open a GIT BASH terminal window  and type the commands below to clone this repo.

    mkdir -p ~/spring-course/my-solutions
    cd ~/spring-course
    git clone <git https url> gitlab

![GIT HTTPS URL](./img/git-url.png)

During the course, solutions will be push:ed to this repo, and you can get
these by a `git pull` operation

    cd ~/spring-course/gitlab
    git pull


# Build Solution/Demo Programs
All programs are ordinary Java programs and can be compiled using any appropriate
tool. Several of the demo & solutions programs has a Gradle build file and can
therefore be built by

    ./gradlew build         # BASH
    gradlew.bat build       # Windows


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

