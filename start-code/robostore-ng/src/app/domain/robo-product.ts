export interface RoboProduct {
    name: string;
    description: string;
    uid: string;
    city: string;
    country: string;
    icon: string;
    image: string;
    price: number;
    count: number;
}
