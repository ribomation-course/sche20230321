import {Component, ViewChild} from '@angular/core';
import {RoboProduct} from "../../domain/robo-product";
import {FormGroup, FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Router, RouterLink} from "@angular/router";

@Component({
    selector: 'app-create',
    standalone: true,
    imports: [
        HttpClientModule, FormsModule, RouterLink
    ],
    templateUrl: './create.page.html',
    styles: []
})
export class CreatePage {
    readonly url = 'http://localhost:8080/api/v1/robo';
    robo: RoboProduct = {
        name: '',
        description: '',
        uid: '',
        city: '',
        country: '',
        icon: '',
        image: '',
        price: 0,
        count: 0
    };
    @ViewChild('form') form: FormGroup | undefined;

    constructor(private http: HttpClient, private router: Router) {
    }

    save() {
        this.http.post<RoboProduct>(this.url, this.robo)
            .subscribe(async (obj) => {
               await this.router.navigate(['/show', this.robo.name]);
            });
    }
}
