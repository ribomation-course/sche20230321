import {Component} from '@angular/core';
import {NgFor} from '@angular/common';

@Component({
    selector: 'home',
    standalone: true,
    imports: [NgFor],
    templateUrl: './home.page.html',
    styles: [''],
})
export class HomePage {
    names = [
        'robo-one', 'robo-two', 'robo-three'
    ];

    uri(name:string):string{
        return `https://robohash.org/${name}.png`;
    }
}
