import {Component, OnInit} from '@angular/core';
import {CurrencyPipe} from '@angular/common';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import "@angular/common/locales/global/sv";


interface Summary {
    count: number;
    total: number;
    zero: number;
}

@Component({
    selector: 'app-summary',
    standalone: true,
    imports: [
        HttpClientModule,
        CurrencyPipe
    ],
    templateUrl: './summary.page.html',
    styleUrls: ['./summary.page.css']
})
export class SummaryPage implements OnInit {
    readonly url='http://localhost:8080/api/v1/robo/summary';
    summary: Summary = {count: 0, total: 0, zero: 0};

    constructor(private http: HttpClient) {
    }

    ngOnInit(): void {
        this.http.get<Summary>(this.url)
            .subscribe(s => this.summary = s);
    }

}
