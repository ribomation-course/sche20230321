import {Component, OnInit} from '@angular/core';
import {CurrencyPipe, JsonPipe, NgIf} from '@angular/common';
import {HttpClient} from "@angular/common/http";
import "@angular/common/locales/global/sv";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {RoboProduct} from "../../domain/robo-product";

@Component({
    selector: 'app-show',
    standalone: true,
    imports: [
        CurrencyPipe, JsonPipe, NgIf, RouterLink
    ],
    templateUrl: './show.page.html',
    styles: []
})
export class ShowPage implements OnInit {
    readonly url = 'http://localhost:8080/api/v1/robo';
    robo: RoboProduct | undefined;

    constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit(): void {
        const name = this.route.snapshot.paramMap.get('name');
        this.http.get<RoboProduct>(`${this.url}/${name}`)
            .subscribe(obj => this.robo = obj);
    }

    remove() {
        const robo: any = this.robo || {};
        this.http.delete<RoboProduct>(`${this.url}/${robo.name}`)
            .subscribe(async (obj) => {
                await this.router.navigate(['/list']);
            });
    }

}
