import {Component, OnInit, OnDestroy} from '@angular/core';
import {CurrencyPipe, NgFor} from "@angular/common";
import "@angular/common/locales/global/sv";
import {RouterLink} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {Subscription} from "rxjs";
import {RoboProduct} from "../../domain/robo-product";
import {SearchService} from "../../services/search.service";

interface PageInfo {
    list: RoboProduct[];
    page: number
}

@Component({
    selector: 'app-list',
    standalone: true,
    imports: [
        NgFor, RouterLink, CurrencyPipe
    ],
    templateUrl: './list.page.html',
    styles: []
})
export class ListPage implements OnInit, OnDestroy {
    readonly url = 'http://localhost:8080/api/v1/robo';
    items: RoboProduct[] = [];
    subs: Subscription | undefined;
    page = 1;

    constructor(private http: HttpClient, private searchSvc: SearchService) {
    }

    ngOnInit(): void {
        this.http.get<RoboProduct[]>(this.url)
            .subscribe(lst => this.items = lst);

        this.subs = this.searchSvc.result$()
            .subscribe(lst => this.items = lst)
    }

    ngOnDestroy(): void {
        if (!!this.subs) {
            this.subs.unsubscribe();
        }
    }

    first() {
        this.http.get<PageInfo>(`${this.url}/first`)
            .subscribe(info => {
                this.items = info.list;
                this.page = info.page;
            });
    }

    prev() {
        this.http.get<PageInfo>(`${this.url}/prev/${this.page}`)
            .subscribe(info => {
                this.items = info.list;
                this.page = info.page;
            });
    }

    next() {
        this.http.get<PageInfo>(`${this.url}/next/${this.page}`)
            .subscribe(info => {
                this.items = info.list;
                this.page = info.page;
            });
    }

    last() {
        this.http.get<PageInfo>(`${this.url}/last`)
            .subscribe(info => {
                this.items = info.list;
                this.page = info.page;
            });
    }

}
