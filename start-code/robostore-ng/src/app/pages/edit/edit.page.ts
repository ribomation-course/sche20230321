import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {RoboProduct} from "../../domain/robo-product";
import {FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgIf} from "@angular/common";

@Component({
    selector: 'app-edit',
    standalone: true,
    imports: [
        FormsModule, NgIf, RouterLink
    ],
    templateUrl: './edit.page.html',
    styles: []
})
export class EditPage implements OnInit {
    readonly url = 'http://localhost:8080/api/v1/robo';
    robo: RoboProduct | undefined;
    @ViewChild('form') form: FormGroup | undefined;

    constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router) {
    }

    ngOnInit(): void {
        const name = this.route.snapshot.paramMap.get('name');
        this.http.get<RoboProduct>(`${this.url}/${name}`)
            .subscribe(obj => this.robo = obj);
    }

    save() {
        const robo: any = this.robo || {};
        this.http.put<RoboProduct>(`${this.url}/${robo.name}`, this.robo)
            .subscribe(async (obj) => {
                await this.router.navigate(['/show', robo.name]);
            });
    }

}
