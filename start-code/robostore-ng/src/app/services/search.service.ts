import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Subject} from "rxjs";
import {RoboProduct} from "../domain/robo-product";

@Injectable({providedIn: 'root'})
export class SearchService {
    readonly url = 'http://localhost:8080/api/v1/robo/search';
    bus = new Subject<RoboProduct[]>();

    constructor(private http: HttpClient) {
    }

    search(phrase: string) {
        this.http.get<RoboProduct[]>(`${this.url}/${phrase}`)
            .subscribe((result: RoboProduct[]) => {
                this.bus.next(result);
            });
    }

    result$() {
        return this.bus;
    }

}
