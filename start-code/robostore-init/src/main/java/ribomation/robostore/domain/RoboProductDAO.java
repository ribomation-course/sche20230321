package ribomation.robostore.domain;

import java.util.List;
import java.util.Optional;

public interface RoboProductDAO {
    String TBL = "ROBO_STORE";

    int count();
    float totalValue();

    default List<RoboProduct> list() {return list(0, 10);}
    List<RoboProduct> list(int offset, int rows);
    List<RoboProduct> search(String phrase);
    List<RoboProduct> outOfStock();

    Optional<RoboProduct> byName(String name);
    boolean exists(String name);

    void insert(RoboProduct robo);
    void insert(List<RoboProduct> list);
    void update(String name, RoboProduct robo);
    void delete(String name);
}
