package ribomation.robostore.domain;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.StringJoiner;


public class RoboProduct {
    private String name;
    private String description;
    private String city;
    private String country;
    private float price;
    private int count;
    private String uid;
    private String icon;
    private String image;

    public static final String[] names = {
            "name", "description", "city", "country", "price", "count", "uid", "icon", "image"
    };

    public static Builder builder() {return new Builder();}

    public static class Builder {
        private final RoboProduct p = new RoboProduct();
        public RoboProduct build() {return p;}

        public Builder name(String val) {p.name=val; return this;}
        public Builder price(float val) {p.price=val; return this;}
        public Builder price(String val) {p.price=Float.parseFloat(val); return this;}
        public Builder count(int val) {p.count=val; return this;}
        public Builder count(String val) {p.count=Integer.parseInt(val); return this;}
        public Builder description(String val) {p.description=val; return this;}
        public Builder uid(String val) {p.uid=val; return this;}
        public Builder city(String val) {p.city=val; return this;}
        public Builder country(String val) {p.country=val; return this;}
        public Builder icon(String val) {p.icon=val; return this;}
        public Builder image(String val) {p.image=val; return this;}
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", RoboProduct.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("city='" + city + "'")
                .add("country='" + country + "'")
                .add("price=" + price)
                .add("count=" + count)
                .add("uid='" + uid + "'")
                .add("icon='" + icon + "'")
                .add("image='" + image + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoboProduct that = (RoboProduct) o;
        return Float.compare(that.price, price) == 0
               && count == that.count
               && name.equals(that.name)
               && Objects.equals(description, that.description)
               && Objects.equals(city, that.city)
               && Objects.equals(country, that.country)
               && Objects.equals(uid, that.uid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, city, country, price, count, uid);
    }

    public String getName() {return name;}
    public String getDescription() {return description;}
    public String getCity() {return city;}
    public String getCountry() {return country;}
    public float getPrice() {return price;}
    public int getCount() {return count;}
    public String getUid() {return uid;}
    public String getIcon() {return icon;}
    public String getImage() {return image;}

    public RoboProduct complete() {
        if (missing(name)) throw new IllegalArgumentException("missing name");
        if (missing(image)) image = mkImage(name);
        if (missing(icon)) icon = mkIcon(name);
        if (missing(uid)) uid = mkUID(name);
        if (missing(price)) price = 10;
        if (missing(count)) count = 1;
        if (missing(description)) description = "Just a silly description";
        if (missing(city)) city = "Stockholm";
        if (missing(country)) country = "Sweden";
        return this;
    }

    private String mkUID(String s) {
        try {
            var alg = MessageDigest.getInstance("SHA-256");
            var bytes = s.getBytes(StandardCharsets.UTF_8);
            var digest = alg.digest(bytes);
            var num = new BigInteger(1, digest);
            var hex = new StringBuilder(num.toString(16));
            while (hex.length() < 64) hex.insert(0, '0');
            return hex.substring(0, 32).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private String mkImage(String s) {return mkPicture(s, 300);}
    private String mkIcon(String s) {return mkPicture(s, 50);}
    private String mkPicture(String s, int size) {
        return String.format("https://robohash.org/%s.png?size=%dx%d", name, size, size);
    }

    private boolean missing(String s) {return s == null || s.isBlank();}
    private boolean missing(int i) {return i < 0;}
    private boolean missing(float i) {return i < 0;}

}
