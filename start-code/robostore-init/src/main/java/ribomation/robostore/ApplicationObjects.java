package ribomation.robostore;

import ribomation.robostore.cli.RoboCommands;
import ribomation.robostore.domain.RoboProductDAO;
import ribomation.robostore.jdbc.BeanPreparator;
import ribomation.robostore.jdbc.H2DatasourceFactory;
import ribomation.robostore.jdbc.JdbcRoboProductDAO;

import javax.sql.DataSource;

public class ApplicationObjects {
    public DataSource getDataSource() {
        return new H2DatasourceFactory().create();
    }

    public BeanPreparator getBeanPreparator() {
        return new BeanPreparator();
    }

    public RoboProductDAO getDao() {
        return new JdbcRoboProductDAO(getDataSource(), getBeanPreparator());
    }

    public RoboCommands getCommands() {
        return new RoboCommands(getDao());
    }
}

