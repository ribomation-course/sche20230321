package ribomation.robostore;

public class RobostoreCLI {
    public static void main(String[] args) {
        var app = new RobostoreCLI();
        app.run(args, new ApplicationObjects());
    }

    void run(String[] args, ApplicationObjects ctx) {
        var runner = new Runner(ctx);
        runner.run(args);
    }
}

